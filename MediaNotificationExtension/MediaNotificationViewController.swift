//
//  MediaNotificationViewController.swift
//  PracticeNotificationExtension
//
//  Created by Cypress on 10/23/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI
import MediaPlayer
import AVKit

enum MediaFileType {
    case audio
    case video
}

class MediaNotificationViewController: UIViewController, UNNotificationContentExtension {

    @IBOutlet private weak var audioContentImageView: UIImageView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var videoContentView: UIView!
    
    private var player: AVPlayer?

    var mediaPlayPauseButtonType: UNNotificationContentExtensionMediaPlayPauseButtonType = .default
    var mediaPlayPauseButtonFrame: CGRect = CGRect(x: 150, y: 86, width: 18, height: 22)
    var mediaPlayPauseButtonTintColor: UIColor = .black

    func didReceive(_ notification: UNNotification) {
        let content = notification.request.content
        
        self.titleLabel.text = content.title
        
        guard let fileUrl = content.attachments.first?.url else { return }
        let mediaFileType = self.mediaFileType(fileUrl)
        
        player = AVPlayer(url: fileUrl)

        if mediaFileType == .video {
            let playerLayer = AVPlayerLayer(player: player)
            playerLayer.frame = self.videoContentView.bounds

            self.videoContentView.layer.addSublayer(playerLayer)
            self.videoContentView.isHidden = false
        } else {
            self.audioContentImageView.isHidden = false
        }
    }
        
    func mediaPlay() {
        self.player?.play()
    }
    
    func mediaPause() {
        self.player?.pause()
    }
    
    private func mediaFileType(_ url: URL) -> MediaFileType {
        if url.pathExtension == "mp4" || url.pathExtension == "3gp" {
            return MediaFileType.video
        }

        return MediaFileType.audio
    }
}
