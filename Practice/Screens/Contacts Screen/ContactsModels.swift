//
//  ContactsModels.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct Contacts {
    struct Fetch {
        struct Request {
        }

        struct Response {
            let contacts: [Contact]
        }

        struct ViewModel {
            let id: String
            let fullName: String?
            let avatarImage: Data?
        }
    }
    
    struct Delete {
        struct Request {
            let contactId: String
        }
    }
    
    struct Select {
        struct Request {
            let contactId: String
        }
    }

    struct AlertInfo {
        let title: String
        let message: String
    }
}
