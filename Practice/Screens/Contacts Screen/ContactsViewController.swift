//
//  ContactsViewController.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

protocol ContactsDisplayLogic: class {
    func showContacts(contacts: [Contacts.Fetch.ViewModel])
    func showAlert(info: Contacts.AlertInfo)
    func endRefreshing()
}

class ContactsViewController: UIViewController, ContactsDisplayLogic {

    @IBOutlet private weak var contactsTableView: UITableView!

    private var interactor: ContactsBusinessLogic?
    var router: (ContactsDataPassing & ContactsRoutingLogic)?

    private var contacts = [Contacts.Fetch.ViewModel]()
    private var refreshControl = UIRefreshControl()

    private static let cellHeight: CGFloat = 75.0
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl()
        interactor?.fetchContacts(request: Contacts.Fetch.Request())
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        interactor?.viewWillAppear()
    }
    
    // MARK: ContactsDisplayLogic

    func showContacts(contacts: [Contacts.Fetch.ViewModel]) {
        self.contacts = contacts
        self.contactsTableView.reloadData()
    }
    
    func showAlert(info: Contacts.AlertInfo) {
        Helper.showAlert(title: info.title, message: info.message, viewController: self)
    }
    
    func endRefreshing() {
        refreshControl.endRefreshing()
    }

    // MARK: Helpers

    private func didSelectContact(id: String) {
        interactor?.didSelectContact(request: Contacts.Select.Request(contactId: id))
        router?.navigateToContactDetailsScreen()
    }
    
    private func deletAction(handler: @escaping () -> Void) -> UIContextualAction {
        let deletAction = UIContextualAction(style: .normal, title: "", handler: { (_, _, success:(Bool) -> Void) in
            handler()
            success(true)
        })

        deletAction.backgroundColor = UIColor.red
        deletAction.image = UIImage(named: Constants.iconNameTrash)
        
        return deletAction
    }
    
    private func editAction(handler: @escaping () -> Void) -> UIContextualAction {
        let editAction = UIContextualAction(style: .normal, title: "", handler: { (_, _, success:(Bool) -> Void) in
            handler()
            success(true)
        })

        editAction.backgroundColor = UIColor.purple
        editAction.image = UIImage(named: Constants.iconNameEdit)

        return editAction
    }
    
    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        contactsTableView.addSubview(refreshControl)
    }

    @objc func refreshList(sender: AnyObject) {
        interactor?.refreshContactList()
    }
}

extension ContactsViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contacts.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.IdContactInfoTableViewCell) as! ContactInfoTableViewCell

        let contact = contacts[indexPath.row]
        cell.updateData(name: contact.fullName, avatar: contact.avatarImage)

        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return ContactsViewController.cellHeight
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        let contact = self.contacts[indexPath.row]
        
        let deletAction = self.deletAction(handler: { [weak self] in
            self?.interactor?.deleteContact(request: Contacts.Delete.Request(contactId: contact.id))
        })

        let editAction = self.editAction(handler: { [weak self] in
            self?.didSelectContact(id: contact.id)
        })

        return UISwipeActionsConfiguration(actions: [deletAction, editAction])
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let contact = self.contacts[indexPath.row]
        self.didSelectContact(id: contact.id)
    }
}

// MARK: -

extension ContactsViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        let viewController = self
        let interactor = ContactsInteractor()
        let presenter = ContactsPresenter()
        let router = ContactsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = self
        router.dataStore = interactor
    }
}

