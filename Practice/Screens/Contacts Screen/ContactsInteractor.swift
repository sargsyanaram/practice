//
//  ContactsInteractor.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol ContactsBusinessLogic {
    func fetchContacts(request: Contacts.Fetch.Request)
    func deleteContact(request: Contacts.Delete.Request)
    func didSelectContact(request: Contacts.Select.Request)
    func refreshContactList()
    func viewWillAppear()
}

protocol ContactsDataStore {
    var selectedContact: Contact? { get set }
}

class ContactsInteractor: ContactsBusinessLogic, ContactsDataStore {
    
    var presenter: ContactsPresentationLogic?
    var selectedContact: Contact?
    
    private var contacts: [Contact]!
    
    // MARK: ContactsBusinessLogic
    
    func fetchContacts(request: Contacts.Fetch.Request) {
        do {
            let allContacts = try ContactsService.getContacts()
            contacts = allContacts.filter { $0.emails.count > 0 }

            presenter?.showContacts(response: Contacts.Fetch.Response(contacts: contacts))
        } catch {
            presenter?.showAlert(info: Contacts.AlertInfo(title: "", message: Constants.messageFailedToLoadContacts))
        }
    }

    func deleteContact(request: Contacts.Delete.Request) {
        do {
            let result: (success: Bool, message: String?) = try ContactsService.deleteContact(id: request.contactId)
            
            if result.success {
                contacts = contacts.filter { $0.id != request.contactId }
                presenter?.showContacts(response: Contacts.Fetch.Response(contacts: contacts))
            } else {
                presenter?.showAlert(info: Contacts.AlertInfo(title: "", message: result.message ?? Constants.messageFailedToDeleteContact))
            }
        } catch {
            presenter?.showAlert(info: Contacts.AlertInfo(title: "", message: Constants.messageFailedToDeleteContact))
        }
    }

    func didSelectContact(request: Contacts.Select.Request) {
        selectedContact = contacts.first { $0.id == request.contactId }
    }
    
    func refreshContactList() {
        do {
            let allContacts = try ContactsService.getContacts()
            contacts = allContacts.filter { $0.emails.count > 0 }

            presenter?.endRefreshing()
            presenter?.showContacts(response: Contacts.Fetch.Response(contacts: contacts))
        } catch {
            presenter?.endRefreshing()
            presenter?.showAlert(info: Contacts.AlertInfo(title: "", message: Constants.messageFailedToLoadContacts))
        }
    }
    
    func viewWillAppear() {
        presenter?.showContacts(response: Contacts.Fetch.Response(contacts: contacts))
    }
}
