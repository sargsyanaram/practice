//
//  ContactsPresenter.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol ContactsPresentationLogic {
    func showContacts(response: Contacts.Fetch.Response)
    func showAlert(info: Contacts.AlertInfo)
    func endRefreshing()
}

class ContactsPresenter: ContactsPresentationLogic {
    weak var viewController: ContactsDisplayLogic?

    func showContacts(response: Contacts.Fetch.Response) {
        let viewModels = response.contacts.map { Contacts.Fetch.ViewModel(id: $0.id, fullName: $0.fullName, avatarImage: $0.avatarData) }

        viewController?.showContacts(contacts: viewModels)
    }
    
    func showAlert(info: Contacts.AlertInfo) {
        viewController?.showAlert(info: info)
    }
    
    func endRefreshing() {
        viewController?.endRefreshing()
    }
}
