//
//  ContactsRouter.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol ContactsRoutingLogic {
    func navigateToContactDetailsScreen()
}

protocol ContactsDataPassing {
    var dataStore: ContactsDataStore? { get }
}

class ContactsRouter: ContactsDataPassing, ContactsRoutingLogic {
    weak var viewController: UIViewController?
    var dataStore: ContactsDataStore?
        
    // MARK: Routing

    func navigateToContactDetailsScreen() {
        guard
            let sourceVC = viewController,
            let sourceDS = dataStore,
            let storyboard = sourceVC.storyboard,
            let destinationVC = storyboard.instantiateViewController(withIdentifier: Constants.IdContactDetailsVC) as? ContactDetailsViewController,
            var destinationDS = destinationVC.router?.dataStore
            else { fatalError(Constants.messageFailedToNavigateToContactDetails) }

        passDataToContactDetailsScreen(source: sourceDS, destination: &destinationDS)
        navigateToViewController(source: sourceVC, destination: destinationVC)
    }

    // MARK: Navigation
    
    func navigateToViewController(source: UIViewController, destination: UIViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }

    // MARK: Passing data
    
    private func passDataToContactDetailsScreen(source: ContactsDataStore, destination: inout ContactDetailsDataStore) {
        destination.contact = source.selectedContact
    }

}

