//
//  ContactDetailsInteractor.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol ContactDetailsBusinessLogic {
    func getContactInfo(request: ContactDetails.Get.Request)
    func editContact(request: ContactDetails.Edit.Request)
}

protocol ContactDetailsDataStore {
    var contact: Contact! { get set }
}

class ContactDetailsInteractor: ContactDetailsBusinessLogic, ContactDetailsDataStore {
    
    var presenter: ContactDetailsPresentationLogic?
    var contact: Contact!

    func getContactInfo(request: ContactDetails.Get.Request) {
        presenter?.presentContactInfo(response: ContactDetails.Get.Response(contact: contact))
    }
    
    func editContact(request: ContactDetails.Edit.Request) {
        self.contact.mapDataFrom(infoFields: request.contactInfo)
        
        do {
            let result: (success: Bool, message: String?) = try ContactsService.editContact(self.contact)
            
            if !result.success {
                editFailed()
            }
        } catch {
            editFailed()
        }
    }
    
    private func editFailed() {
        presenter?.presentContactInfo(response: ContactDetails.Get.Response(contact: contact))
        presenter?.showAlert(info: ContactDetails.AlertInfo(title: "", message: Constants.messageFailedToSaveChanges))
    }
}
