//
//  ContactDetailsModels.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

struct ContactDetails {
    
    struct Get {
        struct Request { }
        
        struct Response {
            var contact: Contact
        }

        struct ViewModel {
            let avatarImage: UIImage?
            var details: [InfoField]
        }

    }

    struct Edit {
        struct Request {
            let contactInfo: [InfoField]
        }
    }

    struct InfoField {
        let id: String
        let type: ContactDetailType
        let label: String?
        var value: String

        enum ContactDetailType {
            case firstName
            case lastName
            case email
            case phoneNumber
        }
    }
    
    struct AlertInfo {
        let title: String
        let message: String
    }
}
