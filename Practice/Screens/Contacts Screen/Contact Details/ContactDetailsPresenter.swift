//
//  ContactDetailsPresenter.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol ContactDetailsPresentationLogic {
    func presentContactInfo(response: ContactDetails.Get.Response)
    func showAlert(info: ContactDetails.AlertInfo)
}

class ContactDetailsPresenter: ContactDetailsPresentationLogic {
    weak var viewController: ContactDetailsDisplayLogic?

    func presentContactInfo(response: ContactDetails.Get.Response) {
        let contact = response.contact
        var contactDetails = [ContactDetails.InfoField]()

        let imageData = contact.avatarData
        let avatarImage = imageData != nil ? UIImage(data: imageData!) : nil

        let firstName = ContactDetails.InfoField(id: UUID().uuidString, type: .firstName, label: nil, value: contact.firstName)
        let lastName = ContactDetails.InfoField(id: UUID().uuidString, type: .lastName, label: nil, value: contact.lastName)
        contactDetails.append(contentsOf: [firstName, lastName])

        for phone in contact.phoneNumbers {
            let contactDetail = ContactDetails.InfoField(id: phone.id, type: .phoneNumber, label: phone.label, value: phone.value)
            contactDetails.append(contactDetail)
        }

        for email in contact.emails {
            let contactDetail = ContactDetails.InfoField(id: email.id, type: .email, label: email.label, value: email.value)
            contactDetails.append(contactDetail)
        }

        viewController?.showContactInfo(contactInfo: ContactDetails.Get.ViewModel(avatarImage: avatarImage, details: contactDetails))
    }
    
    func showAlert(info: ContactDetails.AlertInfo) {
        viewController?.showAlert(info: info)
    }
}
