//
//  ContactDetailsViewController.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

enum RightBarButtonState {
    case edit
    case save
}

protocol ContactDetailsDisplayLogic: class {
    func showContactInfo(contactInfo: ContactDetails.Get.ViewModel)
    func showAlert(info: ContactDetails.AlertInfo)
}

class ContactDetailsViewController: UIViewController, ContactDetailsDisplayLogic {

    @IBOutlet weak var contactInfoTableView: UITableView!
    private var interactor: ContactDetailsBusinessLogic?
    var router: ContactDetailsDataPassing?

    private var contactInfo: ContactDetails.Get.ViewModel!
    private var rightBarButtonState: RightBarButtonState = .edit
    private var rightBarButtonItem: UIBarButtonItem!

    private static let contactAvatarCellHeight: CGFloat = 150.0
    private static let infoCellHeight: CGFloat = 66.0

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        setupComponents()

        interactor?.getContactInfo(request: ContactDetails.Get.Request())
    }
        
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        setupTableView()
    }

    // MARK: ContactDetailsDisplayLogic
    
    func showContactInfo(contactInfo: ContactDetails.Get.ViewModel) {
        self.contactInfo = contactInfo
        self.contactInfoTableView.reloadData()
    }
    
    func showAlert(info: ContactDetails.AlertInfo) {
        Helper.showAlert(title: info.title, message: info.message, viewController: self)
    }
    
    // MARK: Helpers
    
    private func setupTableView() {
        if contactInfoTableView.contentSize.height <= contactInfoTableView.bounds.size.height {
            contactInfoTableView.isScrollEnabled = false
        }
    }
    
    private func setupComponents() {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(closeKeyboard))
        self.view.addGestureRecognizer(tapGesture)
        
        setupNavigationBar()
    }

    private func setupNavigationBar() {
        rightBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(navigationRightBarButtonPressed))
        rightBarButtonItem.image = UIImage(named: Constants.iconNameEdit)
        rightBarButtonItem.tintColor = .black
        
       self.navigationItem.rightBarButtonItem = rightBarButtonItem
    }
        
    private func fieldTypeFrom(contactDetailType: ContactDetails.InfoField.ContactDetailType) -> FieldType {
        let fieldType: FieldType!
        
        switch contactDetailType {
        case .firstName, .lastName:
            fieldType = .defoult
        case .phoneNumber:
            fieldType = .phoneNumber
        case .email:
            fieldType = .email
        }
        
        return fieldType
    }
    
    // MARK: Actions
    
    @objc private func closeKeyboard() {
        self.view.endEditing(true)
    }

    @objc private func navigationRightBarButtonPressed() {
        var title = ""
        var image: UIImage?
        
        switch rightBarButtonState {
        case .edit: do {
            title = Constants.buttonTitleSave
            rightBarButtonState = .save
            }
        case .save: do {
            self.view.endEditing(true)
            
            image = UIImage(named: Constants.iconNameEdit)
            rightBarButtonState = .edit
            
            self.interactor?.editContact(request: ContactDetails.Edit.Request(contactInfo: self.contactInfo.details))
            }
        }
        
        contactInfoTableView.reloadData()
        rightBarButtonItem.title = title
        rightBarButtonItem.image = image
    }
}

extension ContactDetailsViewController: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return contactInfo.details.count + 1 // +1 Avatar cell
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UITableViewCell!
        
        if indexPath.row == 0 {
            cell = contactAvatarCell(tableView: tableView, indexPath: indexPath)
        } else {
            cell = contactDetailCell(tableView: tableView, indexPath: indexPath)
        }

        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? ContactDetailsViewController.contactAvatarCellHeight : ContactDetailsViewController.infoCellHeight
    }

    private func contactDetailCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.IdContactDetailTableViewCell) as! ContactDetailTableViewCell
        let contactInfo = self.contactInfo.details[indexPath.row - 1]
        let contactInfoId = contactInfo.id
        let userInteraction = !(rightBarButtonState == .edit)
        let fieldType = fieldTypeFrom(contactDetailType: contactInfo.type)

        cell.updateData(text: contactInfo.value, isUserInteractionEnabled: userInteraction, fieldType: fieldType) { [weak self] (newValue) in
            guard let contactInfoIndex = self?.contactInfo.details.firstIndex(where: { $0.id == contactInfoId }) else { return }
            self?.contactInfo.details[contactInfoIndex].value = newValue ?? ""
        }

        return cell
    }

    private func contactAvatarCell(tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.IdContactAvatarTableViewCell) as! ContactAvatarTableViewCell
        
        cell.setImage(contactInfo.avatarImage)
        return cell
    }
    
}

// MARK: -

extension ContactDetailsViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        let viewController = self
        let interactor = ContactDetailsInteractor()
        let presenter = ContactDetailsPresenter()
        let router = ContactDetailsRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.dataStore = interactor
    }
}
