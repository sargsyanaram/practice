//
//  ContactDetailsRouter.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol ContactDetailsDataPassing {
    var dataStore: ContactDetailsDataStore? { get }
}

class ContactDetailsRouter: ContactDetailsDataPassing {
    var dataStore: ContactDetailsDataStore?
}
