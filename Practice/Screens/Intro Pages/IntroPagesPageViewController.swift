//
//  IntroPagesPageViewController.swift
//  Practice
//
//  Created by Cypress on 10/8/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class IntroPagesPageViewController: UIPageViewController {
    private var pages = [UIViewController]()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupPageControl()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        guard let subViews = self.view?.subviews else { return }
        var scrollView: UIScrollView?
        var pageControl: UIPageControl?

        for view in subViews {
            if view is UIScrollView {
                scrollView = view as? UIScrollView
            } else if view is UIPageControl {
                pageControl = view as? UIPageControl
            }
        }

        if let scrollView = scrollView, let pageControl = pageControl {
            scrollView.frame = view.bounds
            view.bringSubviewToFront(pageControl)
        }
    }
    
    private func setupPageControl() {
        let appearance = UIPageControl.appearance()
        appearance.pageIndicatorTintColor = UIColor.gray
        appearance.currentPageIndicatorTintColor = UIColor.lightGray
    }
}
