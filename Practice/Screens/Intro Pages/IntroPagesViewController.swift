//
//  IntroPagesViewController.swift
//  Practice
//
//  Created by Cypress on 10/8/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class IntroPagesViewController: UIViewController {

    @IBOutlet private weak var skipButton: UIButton!
    
    private var pageViewController: IntroPagesPageViewController?
    private var loginViewController: LoginViewController!
    private var pages = [UIViewController]()
    
    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        createLoginViewController()
        setRootViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    
        self.navigationController?.navigationBar.isHidden = true
    }
    
    // MARK: Action

    @IBAction func skipButtonPressed(_ sender: UIButton) {
        pageViewController?.setViewControllers([loginViewController], direction: .forward, animated: false, completion: nil)
        setupPageVCForLoginPage()
    }
    
    // MARK: Helpers

    private func setPageVCAsRootViewController() {
        let introPage1 = storyboard!.instantiateViewController(withIdentifier: Constants.introPage1Id)
        let introPage2 = storyboard!.instantiateViewController(withIdentifier: Constants.introPage2Id)
        let introPage3 = storyboard!.instantiateViewController(withIdentifier: Constants.introPage3Id)

        pages.append(contentsOf: [introPage1, introPage2, introPage3, loginViewController])
        pageViewController = IntroPagesPageViewController.init(transitionStyle: .scroll, navigationOrientation: .horizontal)
        pageViewController?.setViewControllers([introPage1], direction: .forward, animated: false, completion: nil)

        pageViewController?.dataSource = self
        pageViewController?.delegate = self

        self.addChild(pageViewController!)
        self.view.addSubviewWithConstraints(subView: pageViewController!.view, left: 0, right: 0, top: 0, bottom: 0)
        pageViewController!.didMove(toParent: self)
        
        self.view.bringSubviewToFront(skipButton)
    }
    
    private func createLoginViewController() {
        loginViewController = storyboard?.instantiateViewController(withIdentifier: Constants.loginViewControllerId) as? LoginViewController
    }
    
    private func setLoginVCAsRootViewController() {
        self.addChild(loginViewController)
        self.view.addSubviewWithConstraints(subView: loginViewController.view, left: 0, right: 0, top: 0, bottom: 0)
        loginViewController.didMove(toParent: self)
    }
    
    private func setRootViewController() {
        let initialLaunch = !UserDefaults.standard.bool(forKey: Constants.introductionPassedKey)

        if initialLaunch {
            setPageVCAsRootViewController()
            UserDefaults.standard.set(true, forKey: Constants.introductionPassedKey)
        } else {
            setLoginVCAsRootViewController()
        }

    }
        
    fileprivate func setupPageVCForLoginPage() {
        skipButton.isHidden = true
        pageViewController?.dataSource = nil
        pageViewController?.delegate = nil
    }
}

// MARK: -

extension IntroPagesViewController: UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        return currentIndex == 0 ? nil : pages[currentIndex - 1]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        let currentIndex = pages.firstIndex(of: viewController)!
        
        if currentIndex == pages.count - 1 {
            setupPageVCForLoginPage()
        }

        return currentIndex == pages.count - 1 ? nil : pages[currentIndex + 1]
    }
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return pages.count - 1 // Do not count login page
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return 0
    }
}
