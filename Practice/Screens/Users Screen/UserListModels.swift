//
//  UserListModels.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct UserList {
    struct Fetch {
        struct Request {
        }

        struct Response {
            let users: [User]
        }
        struct ViewModel {
            let id: Int
            let name: String
            let email: String
        }
    }
    
    struct SelectUser {
        struct Request {
            let userId: Int
        }
    }
    
    struct AlertInfo {
        let title: String
        let message: String
    }
}
