//
//  UserListNetworkingWorker.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

class UserListNetworkingWorker {
    
    static let usersPath = "/users"

    static func fetchUsers(_ completionHandler: @escaping ([User]?, Error?) -> Void) {
        NetworkService.getRequest(url: Constants.baseUrl + UserListNetworkingWorker.usersPath, parameters: nil) { (responseData, error) in
            guard let responseData = responseData else {
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }

                return
            }
            
            do {
                let users = try JSONDecoder().decode([User].self, from: responseData)
    
                DispatchQueue.main.async {
                    completionHandler(users, nil)
                }
            } catch let error {
                DispatchQueue.main.async {
                    assertionFailure(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
}
