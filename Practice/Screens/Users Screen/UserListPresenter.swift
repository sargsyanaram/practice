//
//  UserListPresenter.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol UserListPresentationLogic {
    func presentFetchResult(response: UserList.Fetch.Response)
    func showAlert(info: UserList.AlertInfo)
    func navigateToAlbumsScreen()
    func presentLoadingScreen()
    func hideLoadingScreen()
    func endRefreshing()
}

class UserListPresenter: UserListPresentationLogic
{
    weak var viewController: UserListDisplayLogic?
    
    func presentFetchResult(response: UserList.Fetch.Response) {
        let viewModels = response.users.map { UserList.Fetch.ViewModel(id: $0.id, name: $0.name, email: $0.email) }
        
        viewController?.showUsers(users: viewModels)
    }
    
    func navigateToAlbumsScreen() {
        viewController?.navigateToAlbumsScreen()
    }
    
    func presentLoadingScreen() {
        viewController?.showLoadingScreen()
    }
    
    func hideLoadingScreen() {
        viewController?.hideLoadingScreen()
    }
    
    func showAlert(info: UserList.AlertInfo) {
        viewController?.showAlert(info: info)
    }
    
    func endRefreshing() {
        viewController?.endRefreshing()
    }
}
