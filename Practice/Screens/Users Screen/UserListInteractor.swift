//
//  UserListInteractor.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol UserListBusinessLogic {
    func fetchUsers(request: UserList.Fetch.Request)
    func didSelectUser(request: UserList.SelectUser.Request)
    func refreshUserList()
}

protocol UserListDataStore {
    var selectedUserId: Int? { get set}
}

class UserListInteractor: UserListBusinessLogic, UserListDataStore {
    var presenter: UserListPresentationLogic?
    var selectedUserId: Int?

    func fetchUsers(request: UserList.Fetch.Request) {
        presenter?.presentLoadingScreen()

        UserListNetworkingWorker.fetchUsers { [weak self] (users, error) in
            self?.presenter?.hideLoadingScreen()

            if let users = users {
                self?.presenter?.presentFetchResult(response: UserList.Fetch.Response(users: users))
            } else {
                self?.presenter?.showAlert(info: UserList.AlertInfo(title: "", message: Constants.messageFailedToLoadUsers))
            }
        }
    }
    
    func didSelectUser(request: UserList.SelectUser.Request) {
        selectedUserId = request.userId
        presenter?.navigateToAlbumsScreen()
    }
    
    func refreshUserList() {
        UserListNetworkingWorker.fetchUsers { [weak self] (users, error) in
            self?.presenter?.endRefreshing()
            
            if let users = users {
                self?.presenter?.presentFetchResult(response: UserList.Fetch.Response(users: users))
            } else {
                self?.presenter?.showAlert(info: UserList.AlertInfo(title: "", message: Constants.messageFailedToLoadUsers))
            }
        }
    }
}
