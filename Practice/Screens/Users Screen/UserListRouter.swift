//
//  UserListRouter.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol UserListRoutingLogic {
    func navigateToAlbumsScreen()
}

protocol UserListDataPassing {
    var dataStore: UserListDataStore? { get }
}

class UserListRouter: UserListRoutingLogic, UserListDataPassing {
    weak var viewController: UIViewController?
    var dataStore: UserListDataStore?

    // MARK: Routing

    func navigateToAlbumsScreen() {
        guard
            let sourceVC = viewController,
            let sourceDS = dataStore,
            let storyboard = sourceVC.storyboard,
            let destinationVC = storyboard.instantiateViewController(withIdentifier: Constants.IdAlbumsViewController) as? AlbumListViewController,
            var destinationDS = destinationVC.router?.dataStore
            else { fatalError(Constants.messageFailedToNavigateToAlbums) }

        passDataToAlbumsScreen(source: sourceDS, destination: &destinationDS)
        navigateToViewController(source: sourceVC, destination: destinationVC)
    }

    // MARK: Navigation
    
    func navigateToViewController(source: UIViewController, destination: UIViewController) {
        source.navigationController?.pushViewController(destination, animated: true)
    }
    
    // MARK: Passing data
    
    private func passDataToAlbumsScreen(source: UserListDataStore, destination: inout AlbumListDataStore) {
        destination.userId = source.selectedUserId
    }

}
