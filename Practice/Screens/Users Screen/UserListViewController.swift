//
//  UserListViewController.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

protocol UserListDisplayLogic: class {
    func showUsers(users: [UserList.Fetch.ViewModel])
    func showAlert(info: UserList.AlertInfo)
    func navigateToAlbumsScreen()
    func showLoadingScreen()
    func hideLoadingScreen()
    func endRefreshing()
}

class UserListViewController: UIViewController, UserListDisplayLogic {

    @IBOutlet private weak var userListTableView: UITableView!
    @IBOutlet private weak var loadingView: UIView!
    @IBOutlet private weak var loadingIndicator: UIActivityIndicatorView!
    
    private var interactor: UserListBusinessLogic?
    var router: (UserListRoutingLogic & UserListDataPassing)?

    private var users = [UserList.Fetch.ViewModel]()
    private var refreshControl = UIRefreshControl()

    private static let userInfoCellHeight: CGFloat = 65.0
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl()
        interactor?.fetchUsers(request: UserList.Fetch.Request())
    }
    
    // MARK: UserListDisplayLogic

    func showUsers(users: [UserList.Fetch.ViewModel]) {
        self.users = users
        userListTableView.reloadData()
    }
    
    func navigateToAlbumsScreen() {
        router?.navigateToAlbumsScreen()
    }
    
    func showLoadingScreen() {
        loadingView.isHidden = false
        loadingIndicator.startAnimating()
        self.view.bringSubviewToFront(loadingView)
    }
    
    func hideLoadingScreen() {
        loadingView.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func showAlert(info: UserList.AlertInfo) {
        Helper.showAlert(title: info.title, message: info.message, viewController: self)
    }

    func endRefreshing() {
        refreshControl.endRefreshing()
    }

    // MARK: Helpers
    
    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        userListTableView.addSubview(refreshControl)
    }

    @objc func refreshList(sender: AnyObject) {
        interactor?.refreshUserList()
    }
}

extension UserListViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let userInfoCell = tableView.dequeueReusableCell(withIdentifier: Constants.IdUserInfoTableViewCell) as! UserInfoTableViewCell
        
        let userInfo = users[indexPath.row]
        userInfoCell.updateData(name: userInfo.name, email: userInfo.email)
        
        return userInfoCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UserListViewController.userInfoCellHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let userInfo = users[indexPath.row]
        interactor?.didSelectUser(request: UserList.SelectUser.Request(userId: userInfo.id))
    }
}

// MARK: -

extension UserListViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        let viewController = self
        let interactor = UserListInteractor()
        let presenter = UserListPresenter()
        let router = UserListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = self
        router.dataStore = interactor
    }

}
