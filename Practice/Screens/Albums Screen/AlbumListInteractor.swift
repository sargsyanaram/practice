//
//  AlbumListInteractor.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol AlbumListBusinessLogic {
    func fetchAlbums(request: AlbumList.Fetch.Request)
    func refreshAlbumList()
}

protocol AlbumListDataStore {
    var userId: Int! { get set}
}

class AlbumListInteractor: AlbumListDataStore, AlbumListBusinessLogic {
    var presenter: AlbumListPresentationLogic?
    var userId: Int!

    
    func fetchAlbums(request: AlbumList.Fetch.Request) {
        presenter?.presentLoadingScreen()
        
        AlbumsNetworkingWorker.fetchAlbums(userId: userId) { [weak self] (albums, error) in
            self?.presenter?.hideLoadingScreen()
            
            if let albums = albums {
                self?.presenter?.presentFetchResult(response: AlbumList.Fetch.Response(albums: albums))
            } else {
                self?.presenter?.showAlert(info: AlbumList.AlertInfo(title: "", message: Constants.messageFailedToLoadAlbums))
            }
        }
    }

    func refreshAlbumList() {
        AlbumsNetworkingWorker.fetchAlbums(userId: userId) { [weak self] (albums, error) in
            self?.presenter?.endRefreshing()
            
            if let albums = albums {
                self?.presenter?.presentFetchResult(response: AlbumList.Fetch.Response(albums: albums))
            } else {
                self?.presenter?.showAlert(info: AlbumList.AlertInfo(title: "", message: Constants.messageFailedToLoadAlbums))
            }
        }
    }
}
