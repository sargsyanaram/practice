//
//  AlbumListPresenter.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol AlbumListPresentationLogic {
    func presentFetchResult(response: AlbumList.Fetch.Response)
    func showAlert(info: AlbumList.AlertInfo)
    func presentLoadingScreen()
    func hideLoadingScreen()
    func endRefreshing()
}

class AlbumListPresenter: AlbumListPresentationLogic {
    weak var viewController: AlbumListDisplayLogic?

    func presentFetchResult(response: AlbumList.Fetch.Response) {
        let viewModels = response.albums.map { AlbumList.Fetch.ViewModel(id: $0.id, title: $0.title, album: $0) }

        viewController?.showAlbums(albums: viewModels)
    }
    
    func showAlert(info: AlbumList.AlertInfo) {
        viewController?.showAlert(info: info)
    }
    
    func presentLoadingScreen() {
        viewController?.showLoadingScreen()
    }
    
    func hideLoadingScreen() {
        viewController?.hideLoadingScreen()
    }
    
    func endRefreshing() {
        viewController?.endRefreshing()
    }
}
