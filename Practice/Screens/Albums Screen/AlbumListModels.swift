//
//  AlbumsModels.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct AlbumList {
    struct Fetch {
        struct Request {
        }

        struct Response {
            var albums: [Album]
        }

        struct ViewModel {
            let id: Int
            let title: String
            var album: Album
        }
    }

    struct AlertInfo {
        let title: String
        let message: String
    }
}
