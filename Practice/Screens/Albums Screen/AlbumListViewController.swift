//
//  AlbumListViewController.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

protocol AlbumListDisplayLogic: class {
    func showAlbums(albums: [AlbumList.Fetch.ViewModel])
    func showAlert(info: AlbumList.AlertInfo)
    func showLoadingScreen()
    func hideLoadingScreen()
    func endRefreshing()
}

class AlbumListViewController: UIViewController, AlbumListDisplayLogic {

    @IBOutlet weak var albumsTableView: UITableView!
    @IBOutlet weak var loadingView: UIView!
    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    private var interactor: AlbumListBusinessLogic?
    var router: AlbumListDataPassing?

    private var albums = [AlbumList.Fetch.ViewModel]()
    private var refreshControl = UIRefreshControl()

    private static let albumTableViewCellHeight: CGFloat = 200.0

    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addRefreshControl()
        interactor?.fetchAlbums(request: AlbumList.Fetch.Request())
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        setupNavigationBar()
    }
    
    // MARK: AlbumListDisplayLogic

    func showAlbums(albums: [AlbumList.Fetch.ViewModel]) {
        self.albums = albums
        self.albumsTableView.reloadData()
    }
    
    func showLoadingScreen() {
        loadingView.isHidden = false
        loadingIndicator.startAnimating()
        self.view.bringSubviewToFront(loadingView)
    }

    func hideLoadingScreen() {
        loadingView.isHidden = true
        loadingIndicator.stopAnimating()
    }
    
    func showAlert(info: AlbumList.AlertInfo) {
        Helper.showAlert(title: info.title, message: info.message, viewController: self)
    }
    
    func endRefreshing() {
        refreshControl.endRefreshing()
    }
    
    // MARK: Helpers

    private func setupNavigationBar() {
        self.navigationItem.title = Constants.navigationTitleAlbums
    }
    
    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        albumsTableView.addSubview(refreshControl)
    }

    @objc func refreshList(sender: AnyObject) {
        interactor?.refreshAlbumList()
    }
}

extension AlbumListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return albums.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.IdAlbumTableViewCell) as! AlbumTableViewCell

        let albumViewModel = albums[indexPath.row]
        cell.updateData(title: albumViewModel.title, album: albumViewModel.album)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return AlbumListViewController.albumTableViewCellHeight
    }
}

// MARK: -

extension AlbumListViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        let viewController = self
        let interactor = AlbumListInteractor()
        let presenter = AlbumListPresenter()
        let router = AlbumListRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = self
        router.dataStore = interactor
    }

}

