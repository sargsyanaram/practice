//
//  AlbumTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

protocol AlbumDisplayLogic: class {
    func showPhotos(photos: [AlbumModels.Fetch.ViewModel])
}

class AlbumTableViewCell: UITableViewCell, AlbumDisplayLogic {

    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private weak var photosCollectionView: UICollectionView!
    
    private var interactor: AlbumBusinessLogic?
    private var photos: [AlbumModels.Fetch.ViewModel]?

    private let cellSize = CGSize(width: 150, height: 150)

    override func awakeFromNib() {
        super.awakeFromNib()
        
        photosCollectionView.dataSource = self
        photosCollectionView.delegate = self
    }

    func updateData(title: String?, album: Album) {
        titleLabel.text = title ?? ""
        photosCollectionView.contentOffset.x = 0.0

        let interactor = AlbumInteractor()
        let presenter = AlbumPresenter()
        interactor.presenter = presenter
        presenter.viewController = self

        self.interactor = interactor
        self.interactor?.fetchPhotos(request: AlbumModels.Fetch.Request(album: album))
    }

    // MARK: AlbumDisplayLogic
    
    func showPhotos(photos: [AlbumModels.Fetch.ViewModel]) {
        self.photos = photos
        self.photosCollectionView.reloadData()
    }
}

extension AlbumTableViewCell: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return photos == nil ? 1 : photos!.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell!

        if photos == nil {
            cell = loadingCell(collectionView: collectionView, indexPath: indexPath)
        } else {
            cell = photoCell(collectionView: collectionView, indexPath: indexPath)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return cellSize
    }
    
    private func photoCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.IdPhotoCollectionViewCell, for: indexPath) as! PhotoCollectionViewCell
        
        let photo = photos![indexPath.row]
        cell.updateData(imageUrl: photo.imageUrl)
        
        return cell
    }
    
    private func loadingCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let loadingCell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.IdLoadingCollectionViewCell, for: indexPath) as! LoadingCollectionViewCell

        loadingCell.startAnimating()
        
        return loadingCell
    }
}
