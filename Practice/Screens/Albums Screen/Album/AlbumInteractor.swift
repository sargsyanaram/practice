//
//  AlbumInteractor.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol AlbumBusinessLogic {
    func fetchPhotos(request: AlbumModels.Fetch.Request)
}

class AlbumInteractor: AlbumBusinessLogic {

    var presenter: AlbumPresentationLogic?

    func fetchPhotos(request: AlbumModels.Fetch.Request) {
        let album = request.album
    
        if album.photos == nil {
            AlbumsNetworkingWorker.fetchPhotosFor(album: album.id) { [weak self] (photos, error) in
                assert(photos != nil, Constants.messageFailedToLoadPhotos)

                album.photos = photos
                self?.presenter?.presentFetchResult(response: AlbumModels.Fetch.Response(photos: photos))
            }
        } else {
            self.presenter?.presentFetchResult(response: AlbumModels.Fetch.Response(photos: album.photos))
        }
    }
}
