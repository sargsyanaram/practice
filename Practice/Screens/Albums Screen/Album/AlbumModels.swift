//
//  AlbumModels.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct AlbumModels {
    struct Fetch {
        struct Request {
            let album: Album
        }

        struct Response {
            var photos: [Photo]?
        }

        struct ViewModel {
            let imageUrl: String
        }
    }
}
