//
//  AlbumPresenter.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol AlbumPresentationLogic {
    func presentFetchResult(response: AlbumModels.Fetch.Response)
}

class AlbumPresenter: AlbumPresentationLogic {
    weak var viewController: AlbumDisplayLogic?

    func presentFetchResult(response: AlbumModels.Fetch.Response) {
        var viewModels = [AlbumModels.Fetch.ViewModel]()

        if let photos = response.photos {
            viewModels = photos.map { AlbumModels.Fetch.ViewModel(imageUrl: $0.thumbnailUrl) }
        }

        viewController?.showPhotos(photos: viewModels)
    }
}
