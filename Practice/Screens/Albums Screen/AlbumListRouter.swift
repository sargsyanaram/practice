//
//  AlbumListRouter.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol AlbumListDataPassing {
    var dataStore: AlbumListDataStore? { get }
}

class AlbumListRouter: AlbumListDataPassing {
    weak var viewController: UIViewController?
    var dataStore: AlbumListDataStore?
}
