//
//  AlbumsNetworkingWorker.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

class AlbumsNetworkingWorker {
    private static let albumsPath = "/albums"
    private static let photosPath = "/photos"

    static func fetchAlbums(userId: Int, completionHandler: @escaping ([Album]?, Error?) -> Void) {
        let parameters = [Constants.keyUserId : String(userId)]
        
        NetworkService.getRequest(url: Constants.baseUrl + AlbumsNetworkingWorker.albumsPath, parameters: parameters) {  (responseData, error) in
            guard let responseData = responseData else {
                DispatchQueue.main.async {
                    completionHandler(nil, error)
                }
                return
            }

            do {
                let albums = try JSONDecoder().decode([Album].self, from: responseData)
                
                DispatchQueue.main.async {
                    completionHandler(albums, nil)
                }
            } catch let error {
                DispatchQueue.main.async {
                    assertionFailure(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }

    static func fetchPhotosFor(album albumId: Int, completionHandler: @escaping ([Photo]?, Error?) -> Void) {
        let parameters = [Constants.keyAlbumId : String(albumId)]
        
        NetworkService.getRequest(url: Constants.baseUrl + AlbumsNetworkingWorker.photosPath, parameters: parameters) { (responseData, error) in
            guard let responseData = responseData else {
                completionHandler(nil, error)
                return
            }
            
            do {
                let photos = try JSONDecoder().decode([Photo].self, from: responseData)

                DispatchQueue.main.async {
                    completionHandler(photos, nil)
                }
            } catch let error {
                DispatchQueue.main.async {
                    assertionFailure(error.localizedDescription)
                    completionHandler(nil, error)
                }
            }
        }
    }
}
