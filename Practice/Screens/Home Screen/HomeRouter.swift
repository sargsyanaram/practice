//
//  HomeRouter.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol HomenRoutingLogic {
    func routeToLoginPage()
}

class HomeRouter: HomenRoutingLogic
{
    weak var viewController: UIViewController!

    // MARK: Routing

    func routeToLoginPage() {
        navigateToPreviousViewController(viewController: viewController)
    }
    
    // MARK: Navigation
        
    func navigateToPreviousViewController(viewController: UIViewController) {
        viewController.navigationController?.popViewController(animated: true)
    }
}
