//
//  HomeViewController.swift
//  Practice
//
//  Created by Cypress on 10/9/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit
import SideMenu

enum SelectedMenuItem: Int {
    case Users = 0
    case Contacts
    case Media
}

class HomeViewController: UIViewController {

    @IBOutlet private weak var contentView: UIView!
    private var router: HomenRoutingLogic?

    private var screens = [Int: UIViewController]()
    private var sideMenuNavigationController: SideMenuNavigationController!
    private var menuItems: [MenuItem] {
        return [MenuItem(name: "Users", image: UIImage(named: Constants.iconNameUsers)!),
                MenuItem(name: "Contacts", image: UIImage(named: Constants.iconNameContacts)!),
                MenuItem(name: "Media", image: UIImage(named: Constants.iconNameMedia)!)]
    }
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupSideMenu()
        setupNavigationBar()
    }
    
    // MARK: Side menu
    
    private func setupSideMenu() {
        let sideMenuViewController = storyboard?.instantiateViewController(withIdentifier: Constants.IdSideMenuViewController) as! SideMenuViewController
        sideMenuViewController.delegate = self
        sideMenuViewController.menuItems = self.menuItems

        sideMenuNavigationController = SideMenuNavigationController(rootViewController: sideMenuViewController)
        sideMenuNavigationController.leftSide = true
        sideMenuNavigationController.navigationBar.isHidden = true
        sideMenuNavigationController.presentationStyle = .menuSlideIn
        
        SideMenuManager.default.leftMenuNavigationController = sideMenuNavigationController

        // Prevent status bar area from turning black when menu appears:
        sideMenuNavigationController.statusBarEndAlpha = 0
    }

    private func showSideMenu() {
        self.present(sideMenuNavigationController, animated: true, completion: nil)
    }
    
    private func hideSideMenu(animated: Bool) {
        DispatchQueue.main.async(execute: {
            self.dismiss(animated: animated)
        })
    }
    
    // MARK: Helpers
    
    private func setupNavigationBar() {
        self.navigationController?.navigationBar.isHidden = false

        let leftBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: #selector(menuButtonPressed))
        leftBarButtonItem.image = UIImage(named: Constants.iconNameMenu)
        leftBarButtonItem.tintColor = .black
        
       self.navigationItem.leftBarButtonItem = leftBarButtonItem
    }

    @objc private func menuButtonPressed() {
        showSideMenu()
    }
    
    // MARK: -
        
    private func showScreen(screen: SelectedMenuItem, identifire: String) {
        let index = screen.rawValue
        var viewController = screens[index]

        if viewController == nil {
            viewController = storyboard?.instantiateViewController(withIdentifier: identifire)
            screens[index] = viewController
        }

        setContentViewController(viewController!)
    }
    
    private func setContentViewController(_ viewController: UIViewController) {
        let screenAlreadyShown = self.children.contains(viewController)

        if !screenAlreadyShown {
            self.removeChildeViewController()
            self.contentView.subviews.forEach({ $0.removeFromSuperview() })

            self.addChild(viewController)
            self.contentView.addSubviewWithConstraints(subView: viewController.view, left: 0, right: 0, top: 0, bottom: 0)
            viewController.didMove(toParent: self)
        }
    }
}

// MARK: -

extension HomeViewController: SideMenuDelegate {
    
    func sideMenu(didSelectItemAt index: Int) {
        hideSideMenu(animated: true)
        
        switch index {
        case SelectedMenuItem.Users.rawValue: do {
            self.title = Constants.navigationTitleUsers
            showScreen(screen: SelectedMenuItem.Users, identifire: Constants.userListViewControllerId)
            }
        case SelectedMenuItem.Contacts.rawValue: do {
            self.title = Constants.navigationTitleContacts
            showScreen(screen: SelectedMenuItem.Contacts, identifire: Constants.IdContactsViewController)
            }
        case SelectedMenuItem.Media.rawValue: do {
            self.title = Constants.navigationTitleMedia
            showScreen(screen: SelectedMenuItem.Media, identifire: Constants.IdMediaViewController)
            }
        default: break
        }
    }
    
    func logoutButtonPressed() {
        hideSideMenu(animated: false)
        router?.routeToLoginPage()
    }
}

// MARK: -

extension HomeViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup() {
        let viewController = self
        let router = HomeRouter()
        viewController.router = router
        router.viewController = viewController
    }

}

