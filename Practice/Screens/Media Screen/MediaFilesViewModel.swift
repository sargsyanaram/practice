//
//  MediaFilesViewModel.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import RxCocoa

protocol MediaFilesBusinessLogic {
    func fetchMediaFiles()
    func fetchMediaFiles(completionHandler: ((Bool, String?) -> Void)?)
    func didSelectMediaFile(_ mediaFile: MediaFile)
}

class MediaFilesViewModel: MediaFilesBusinessLogic {

    weak var dataSource : GenericDataSource<MediaFile>?
    private static let filesPath = "practiceMedia"
    
    init(dataSource : GenericDataSource<MediaFile>?) {
        self.dataSource = dataSource
    }

    func fetchMediaFiles() {
        self.fetchMediaFiles(completionHandler: nil)
    }

    func fetchMediaFiles(completionHandler: ((Bool, String?) -> Void)?) {
        do {
            let mediaFiles = try MediaService.getMediaFilesFrom(path: MediaFilesViewModel.filesPath)
            self.dataSource?.data.accept(mediaFiles)

            completionHandler?(true, nil)
        } catch let error {
            completionHandler?(false, error.localizedDescription)
        }
    }

    func didSelectMediaFile(_ mediaFile: MediaFile) {
        let player = mediaFile.player
        guard let mediaFiles = self.dataSource?.data.value else { return }

        if player.state == .isPlaying {
            player.pause()

        } else {
            if player.state == .notStarted {
                scheduleNotification(mediaFile: mediaFile)
            }

            player.play { [weak self] in
                self?.dataSource?.data.accept(mediaFiles)
            }
        }

        self.dataSource?.data.accept(mediaFiles)
    }
    
    private func scheduleNotification(mediaFile: MediaFile) {
        UNUserNotificationCenter.current().getNotificationSettings { [weak self] (settings) in
            
            guard settings.authorizationStatus == .authorized else { return }
            let content = UNMutableNotificationContent()
            
            if let attachment = self?.notificationAttachmentFor(mediaFile: mediaFile) {
                content.attachments = [attachment]
            }

            content.categoryIdentifier = Constants.categoryIdentifierMediaNotification
            content.title = mediaFile.name
                                
            let uuidString = UUID().uuidString
            let request = UNNotificationRequest(identifier: uuidString, content: content, trigger: nil)
                        
            UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
        }
    }
    
    private func notificationAttachmentFor(mediaFile: MediaFile) -> UNNotificationAttachment? {
        var attachment: UNNotificationAttachment?

        if let tmpFileUrl = FileManager.default.copyItemToTemporaryDirectory(at: mediaFile.fileUrl) {
            attachment = try? UNNotificationAttachment(identifier: mediaFile.id, url: tmpFileUrl, options: .none)
        }

        return attachment
    }
}
