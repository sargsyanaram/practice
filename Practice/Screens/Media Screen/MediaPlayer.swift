//
//  MediaPlayer.swift
//  Practice
//
//  Created by Cypress on 10/22/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import MediaPlayer

enum MediaPlayerState {
    case notStarted
    case isPlaying
    case paused
}

class MediaPlayer: NSObject, AVAudioPlayerDelegate {
    var state: MediaPlayerState
    private let player: AVPlayer
    private var completionHandler: (() -> Void)?
    
    init(player: AVPlayer) {
        state = .notStarted
        self.player = player
    }
    
    func play(didReachTheEnd completionHandler: @escaping () -> Void) {
        if self.state == .isPlaying {
            return
        }
        
        snedStopNotification()
        registrForStopNotification()

        player.play()
        state = .isPlaying
        self.completionHandler = completionHandler
    }

    func pause() {
        if self.state == .paused || self.state == .notStarted {
            return
        }
        
        player.pause()
        state = .paused

        unregistrForStopNotification()
        self.completionHandler = nil
    }
    
    private func registrForStopNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(didReceiveStopNotification), name: NSNotification.Name(rawValue: Constants.notificationNameStopMadiaPlaying), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(didReachTheEnd), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime,
                                                         object: nil)
    }
    
    private func unregistrForStopNotification () {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name(rawValue: Constants.notificationNameStopMadiaPlaying), object: nil)
    }
    
    private func snedStopNotification() {
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: Constants.notificationNameStopMadiaPlaying), object: nil)
    }
    
    @objc private func didReceiveStopNotification() {
        pause()
    }
    
    @objc private func didReachTheEnd(notification: NSNotification) {
        player.pause()
        player.seek(to: CMTime.zero)
        state = .paused
        
        unregistrForStopNotification()
        completionHandler?()
        self.completionHandler = nil
    }
}
