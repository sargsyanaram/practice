//
//  MediaFilesViewController.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import UserNotifications

class MediaFilesViewController: UIViewController {

    @IBOutlet private weak var mediaFilesTableView: UITableView!

    private let dataSource = MediaFilesDataSource()
    private let disposeBag = DisposeBag()
    private lazy var viewModel : MediaFilesBusinessLogic = { MediaFilesViewModel(dataSource: self.dataSource) }()
    private var refreshControl = UIRefreshControl()

    private static let cellHeight: CGFloat = 70
    
    // MARK: View lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()

        addObservers()
        addRefreshControl()
        setupDataSource()
        setupNotificationCategories()

        viewModel.fetchMediaFiles()
    }
    
    // MARK: Helpers
    
    private func addObservers() {
        NotificationCenter.default.addObserver(forName: UIApplication.willEnterForegroundNotification, object: nil, queue: .main) { [weak self] notification in
            
            self?.mediaFilesTableView.reloadData()
        }
    }
    
    private func setupDataSource() {
        mediaFilesTableView.dataSource = dataSource
        
        dataSource.data.asObservable().subscribe ({ [weak self] (_) in
            self?.mediaFilesTableView.reloadData()
        }).disposed(by: disposeBag)
    }
    
    private func setupNotificationCategories () {
        let notifCategory = UNNotificationCategory(identifier: Constants.categoryIdentifierMediaNotification, actions: [], intentIdentifiers: [], options: [])
        UNUserNotificationCenter.current().setNotificationCategories([notifCategory])
    }

    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(refreshList), for: UIControl.Event.valueChanged)
        mediaFilesTableView.addSubview(refreshControl)
    }

    @objc func refreshList(sender: AnyObject) {
        viewModel.fetchMediaFiles { [weak self] (success, error) in
            self?.refreshControl.endRefreshing()
        }
    }
}

// MARK: -
extension MediaFilesViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let mediaFile = self.dataSource.data.value[indexPath.row]
        viewModel.didSelectMediaFile(mediaFile)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MediaFilesViewController.cellHeight
    }
}
