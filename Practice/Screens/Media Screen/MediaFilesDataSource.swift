//
//  MediaFilesDataSource.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

class MediaFilesDataSource: GenericDataSource<MediaFile>, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.value.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: Constants.IdMediaItemTableViewCell) as! MediaItemTableViewCell

        let mediaFile = data.value[indexPath.row]
        cell.updateData(fileName: mediaFile.name, type: mediaFile.type, state: mediaFile.player.state)

        return cell
    }
}
