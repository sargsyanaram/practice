//
//  LoginModels.swift
//  Practice
//
//  Created by Cypress on 10/10/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct Login {
    struct Request {
        var userName: String?
        var password: String?
    }
}
