//
//  LoginViewController.swift
//  Practice
//
//  Created by Cypress on 10/8/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit
import Foundation

protocol LoginDisplayLogic: class {
    func goToHomePage()
    func showAlert(title: String, message: String)
}

class LoginViewController: UIViewController, LoginDisplayLogic {

    @IBOutlet private weak var userNameTextField: UITextField!
    @IBOutlet private weak var passwordTextField: UITextField!
    @IBOutlet private weak var signInButton: UIButton!
    
    private var interactor: LoginBusinessLogic?
    private var router: LoginRoutingLogic?

    // MARK: View lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupComponents()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        interactor?.viewDidAppear()
    }
    
    // MARK: Login
    
    @IBAction private func signInButtonPressed(_ sender: UIButton) {
        closeKeyboard()

        let request = Login.Request(userName: userNameTextField.text, password: passwordTextField.text)
        self.interactor?.login(request)
    }
    
    // MARK: DisplayLogic
    
    func showAlert(title: String, message: String) {
        Helper.showAlert(title: title, message: message, viewController: self)
    }
    
    func goToHomePage() {
        clearFields()
        router?.routeToHomePage()
    }
    
    // MARK: Helpers
    
    private func setupComponents() {
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(closeKeyboard))
        self.view.addGestureRecognizer(tapGesture)
                
        signInButton.layer.cornerRadius = signInButton.bounds.size.height / 4
        signInButton.layer.masksToBounds = true

        let userNameView = UIView()
        let passwordView = UIView()
        let userNameImage = UIImage(named: Constants.iconNameUser)?.withRenderingMode(.alwaysTemplate)
        let passwordImage = UIImage(named: Constants.iconNamePassword)?.withRenderingMode(.alwaysTemplate)
        let userNameImageView = UIImageView(image: userNameImage)
        let passwordImageView = UIImageView(image: passwordImage)

        userNameImageView.tintColor = .gray
        passwordImageView.tintColor = .gray
        userNameView.addSubviewWithConstraints(subView: userNameImageView, left: 4, right: 4, top: 4, bottom: 6)
        passwordView.addSubviewWithConstraints(subView: passwordImageView, left: 4, right: 4, top: 4, bottom: 6)
        
        userNameTextField.leftViewMode = .always
        passwordTextField.leftViewMode = .always
        userNameTextField.leftView = userNameView
        passwordTextField.leftView = passwordView
    }
    
    @objc private func closeKeyboard() {
        self.view.endEditing(true)
    }

    private func clearFields() {
        userNameTextField.text = ""
        passwordTextField.text = ""
    }
}

// MARK: -

extension LoginViewController {

    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }

    private func setup()
    {
        let viewController = self
        let interactor = LoginInteractor()
        let presenter = LoginPresenter()
        let router = LoginRouter()
        viewController.interactor = interactor
        viewController.router = router
        interactor.presenter = presenter
        presenter.viewController = viewController
        router.viewController = viewController
    }

}
