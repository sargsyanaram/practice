//
//  LoginInteractor.swift
//  Practice
//
//  Created by Cypress on 10/10/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol LoginBusinessLogic {
    func login(_ request: Login.Request)
    func viewDidAppear()
}

class LoginInteractor: LoginBusinessLogic
{
    var presenter: LoginPresentationLogic?
    
    func viewDidAppear() {
        if !Reachability.isConnectedToNetwork() {
            presenter?.showAlert(title: "", message: Constants.messageTurnOnInternet)
        }
    }

    func login(_ request: Login.Request) {
        let isValidDataPassed = self.isValidDataPassed(userName: request.userName, password: request.password)
        
        if isValidDataPassed && Reachability.isConnectedToNetwork() {
            presenter?.presentHomePage()
        } else {
            presenter?.showAlert(title: "", message: Constants.messageLoginFailed)
        }
    }

    private func isValidDataPassed(userName: String?, password: String?) -> Bool {
        if let userName = userName, let password = password {
            if password.digitsOnly.count > 6 && Helper.isValidEmail(emailStr: userName) {
                return true
            }
        }
        
        return false
    }
}
