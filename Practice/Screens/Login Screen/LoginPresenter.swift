//
//  LoginPresenter.swift
//  Practice
//
//  Created by Cypress on 10/10/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

protocol LoginPresentationLogic
{
    func presentHomePage()
    func showAlert(title: String, message: String)
}

class LoginPresenter: LoginPresentationLogic
{
    weak var viewController: LoginDisplayLogic?
    
    func showAlert(title: String, message: String) {
        viewController?.showAlert(title: title, message: message)
    }
    
    func presentHomePage() {
        viewController?.goToHomePage()
    }
}
