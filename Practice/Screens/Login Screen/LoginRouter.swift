//
//  LoginRouter.swift
//  Practice
//
//  Created by Cypress on 10/10/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import UIKit

protocol LoginRoutingLogic {
    func routeToHomePage()
}

class LoginRouter: LoginRoutingLogic
{
    weak var viewController: UIViewController?

    // MARK: Routing

    func routeToHomePage() {
        let storyboard = UIStoryboard(name: Constants.mainStoryboardName, bundle: nil)
        let destinationVC = storyboard.instantiateViewController(withIdentifier: Constants.homeViewControllerId)

        navigateToViewController(source: viewController!, destination: destinationVC)
    }

    // MARK: Navigation
    
    func navigateToViewController(source: UIViewController, destination: UIViewController) {
        source.show(destination, sender: nil)
    }
}
