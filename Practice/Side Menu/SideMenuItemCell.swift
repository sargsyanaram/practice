//
//  SideMenuItemCell.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class SideMenuItemCell: UITableViewCell {

    @IBOutlet private weak var iconImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!

    func updateData(name: String?, image: UIImage?) {
        nameLabel.text = name
        iconImageView?.image = image
    }

}
