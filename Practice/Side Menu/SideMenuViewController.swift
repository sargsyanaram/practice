//
//  SideMenuViewController.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

struct MenuItem {
    let name: String
    let image: UIImage
}

protocol SideMenuDelegate: class {
    func logoutButtonPressed()
    func sideMenu(didSelectItemAt index: Int)
}

class SideMenuViewController: UIViewController {

    @IBOutlet private weak var menuItemsTableView: UITableView!

    weak var delegate: SideMenuDelegate?
    var menuItems: [MenuItem]?
    
    private let rowHeight: CGFloat = 55.0

    @IBAction private func logOutButtonPressed(_ sender: UIButton) {
        delegate?.logoutButtonPressed()
    }
}

extension SideMenuViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuItems?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let menuItemCell = tableView.dequeueReusableCell(withIdentifier: Constants.IdSideMenuItemCell) as! SideMenuItemCell
        
        let menuItem = menuItems![indexPath.row]
        menuItemCell.updateData(name: menuItem.name, image: menuItem.image)
        
        return menuItemCell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return rowHeight
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        delegate?.sideMenu(didSelectItemAt: indexPath.row)
    }
}
