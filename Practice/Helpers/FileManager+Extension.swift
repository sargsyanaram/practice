//
//  FileManager+Extension.swift
//  Practice
//
//  Created by Cypress on 10/23/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

extension FileManager {
    
    open func copyItemToTemporaryDirectory(at srcURL: URL) -> URL? {
        let temporaryUrl = temporaryDirectory
        let lastPathComp = srcURL.lastPathComponent
        let fileUrl = temporaryUrl.appendingPathComponent(lastPathComp)
        
        do {
            if !FileManager.default.fileExists(atPath: srcURL.path) {
                return nil
            }

            try FileManager.default.copyItem(at: srcURL, to: fileUrl)
        } catch (let error) {
            print("Cannot copy item at \(srcURL) \(error)")
            return nil
        }
        
        return fileUrl
    }

}
