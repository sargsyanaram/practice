//
//  UIViewControllerExtension.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

extension UIViewController {
    func removeChildeViewController() {
        let viewControllers:[UIViewController] = self.children

        for viewContoller in viewControllers {
            viewContoller.willMove(toParent: nil)
            viewContoller.view.removeFromSuperview()
            viewContoller.removeFromParent()
        }
    }
}
