//
//  GenericDataSource.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import RxCocoa
import RxSwift

class GenericDataSource<T> : NSObject {
    var data: BehaviorRelay<[T]> = BehaviorRelay(value: [])
}
