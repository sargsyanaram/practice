//
//  String+Extension.swift
//  Practice
//
//  Created by Cypress on 10/23/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

extension String {
    var digitsOnly: String {
        let pattern = UnicodeScalar("0")..."9"
        return String(unicodeScalars
            .compactMap { pattern ~= $0 ? Character($0) : nil })
    }
}
