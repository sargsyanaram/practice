//
//  UIViewExtentions.swift
//  Practice
//
//  Created by Cypress on 10/11/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

extension UIView {
    
    func addSubviewWithConstraints(subView: UIView, left: CGFloat, right: CGFloat, top: CGFloat, bottom: CGFloat) {
        subView.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(subView)

        NSLayoutConstraint(item: subView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1.0, constant: left).isActive = true
        NSLayoutConstraint(item: subView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1.0, constant: -right).isActive = true
        NSLayoutConstraint(item: subView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1.0, constant: top).isActive = true
        NSLayoutConstraint(item: subView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1.0, constant: -bottom).isActive = true
    }
}
