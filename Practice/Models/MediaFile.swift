//
//  MediaFile.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

enum MediaFileType {
    case audio
    case video
}

class MediaFile {
    let id = UUID().uuidString
    let name: String
    var type: MediaFileType
    let player: MediaPlayer
    let fileUrl: URL

    init(name: String, type: MediaFileType, player: MediaPlayer, fileUrl: URL) {
        self.name = name
        self.type = type
        self.player = player
        self.fileUrl = fileUrl
    }
}
