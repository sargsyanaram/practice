//
//  User.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

typealias Codable = Encodable & Decodable

struct User: Codable {
    let id: Int
    let name: String
    let username: String
    let email: String
    let address: Address
    let phone: String
    let website: String
    let company: Company
}

struct Company: Codable {
    let name: String
    let catchPhrase: String
    let bs: String
}

struct Address: Codable {
    let street: String
    let suite: String
    let city: String
    let zipcode: String
    let geo: Geolocation
}

struct Geolocation: Codable {
    let lat: String
    let lng: String
}
