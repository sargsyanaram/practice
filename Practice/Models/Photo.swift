//
//  Photo.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

struct Photo: Codable {
    let albumId: Int
    let id: Int
    let title: String
    let url: String
    let thumbnailUrl: String
}
