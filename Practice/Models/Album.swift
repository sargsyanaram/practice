//
//  Album.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

class Album: Codable {
    let userId: Int
    let id: Int
    let title: String
    var photos: [Photo]?
    
    enum CodingKeys: String, CodingKey {
        case userId
        case id
        case title
    }

    required init(from decoder: Decoder) throws {
      let container = try decoder.container(keyedBy: CodingKeys.self)
        userId = try container.decode(Int.self, forKey: .userId)
        id = try container.decode(Int.self, forKey: .id)
        title = try container.decode(String.self, forKey: .title)
    }
}
