//
//  Contact.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import ContactsUI

class Contact: NSObject {

    let id: String
    var firstName: String
    var lastName: String
    var avatarData: Data?
    var phoneNumbers = [ContactDetail]()
    var emails = [ContactDetail]()
    var fullName: String { firstName + " " + lastName }

    init(contact: CNContact) {
        id = contact.identifier
        firstName = contact.givenName
        lastName = contact.familyName
        avatarData = contact.imageData
        phoneNumbers = contact.phoneNumbers.map { ContactDetail(label: $0.label, value: $0.value.stringValue) }
        emails = contact.emailAddresses.map { ContactDetail(label: $0.label, value: $0.value as String) }
    }

    func mapDataFrom(infoFields: [ContactDetails.InfoField]) {
        for field in infoFields {
            
            switch field.type {
            case .firstName:
                self.firstName = field.value
            case .lastName:
                self.lastName = field.value
            case .phoneNumber: do {
                guard let index = self.phoneNumbers.firstIndex(where: { $0.id == field.id }) else { return }
                self.phoneNumbers[index].value = field.value
                }
            case .email: do {
                guard let index = self.emails.firstIndex(where: { $0.id == field.id }) else { return }
                self.emails[index].value = field.value
                }
            }
        }
    }
}

struct ContactDetail {
    let id = UUID().uuidString
    let label: String?
    var value: String
}
