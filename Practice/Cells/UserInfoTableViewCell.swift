//
//  UserInfoTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class UserInfoTableViewCell: UITableViewCell {

    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var emailLabel: UILabel!

    func updateData(name: String?, email: String?) {
        nameLabel.text = name ??  ""
        emailLabel?.text = email ?? ""
    }
}
