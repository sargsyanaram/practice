//
//  LoadingCollectionViewCell.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class LoadingCollectionViewCell: UICollectionViewCell {

    @IBOutlet private weak var loadingActivityIndicator: UIActivityIndicatorView!

    func startAnimating() {
        loadingActivityIndicator.startAnimating()
    }
}
