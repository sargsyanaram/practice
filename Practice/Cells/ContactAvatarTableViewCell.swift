//
//  ContactAvatarTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class ContactAvatarTableViewCell: UITableViewCell {

    @IBOutlet private weak var avatarImageView: UIImageView!
    
    func setImage(_ image: UIImage?) {
        avatarImageView.image = image
        avatarImageView.contentMode = .scaleAspectFill

        if image != nil {
            avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
            avatarImageView.layer.masksToBounds = true
            avatarImageView.image = image
        } else {
            avatarImageView.layer.cornerRadius = 0.0
            avatarImageView.layer.masksToBounds = false
            avatarImageView.image = UIImage(named: Constants.iconNameUserBig)
        }
    }
}
