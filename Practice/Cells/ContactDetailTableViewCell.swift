//
//  ContactDetailTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

enum FieldType {
    case defoult
    case phoneNumber
    case email
}

class ContactDetailTableViewCell: UITableViewCell, UITextFieldDelegate {

    @IBOutlet private weak var contactInfoTextField: UITextField!
    private var didEndEditing: ((String?) -> Void)!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        contactInfoTextField.delegate = self
    }

    func updateData(text: String, isUserInteractionEnabled: Bool, fieldType: FieldType, didEndEditing: @escaping (String?) -> Void) {
        contactInfoTextField.text = text
        contactInfoTextField.isUserInteractionEnabled = isUserInteractionEnabled
        contactInfoTextField.borderStyle = isUserInteractionEnabled ? .roundedRect : .none
        self.didEndEditing = didEndEditing

        switch fieldType {
        case .defoult:
            contactInfoTextField.keyboardType = .default
        case .email:
            contactInfoTextField.keyboardType = .emailAddress
        case .phoneNumber:
            contactInfoTextField.keyboardType = .phonePad
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        self.didEndEditing(textField.text)
    }
}
