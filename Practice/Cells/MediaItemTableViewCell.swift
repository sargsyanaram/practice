//
//  MediaItemTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class MediaItemTableViewCell: UITableViewCell {

    @IBOutlet private weak var fileTypeImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var playButton: UIButton!

    func updateData(fileName: String?, type: MediaFileType, state: MediaPlayerState) {
        let playButtonImageName = state == .isPlaying ? Constants.iconNamePause : Constants.iconNamePlay
        var imageName: String!

        switch type {
        case .audio:
            imageName = Constants.iconNameAudio
        case .video:
            imageName = Constants.iconNameVideo
        }
        
        nameLabel.text = fileName ?? ""
        fileTypeImageView.image = UIImage(named: imageName)
        playButton.imageView?.image = UIImage(named: playButtonImageName)
    }
}
