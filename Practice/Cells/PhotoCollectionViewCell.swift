//
//  PhotoCollectionViewCell.swift
//  Practice
//
//  Created by Cypress on 10/15/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit
import SDWebImage

class PhotoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet private weak var photoImageView: UIImageView!

    func updateData(imageUrl: String?) {
        let placeholderImage = UIImage(named: Constants.imagePlaceholderName)

        if let imageUrl = imageUrl {
            photoImageView.sd_setImage(with: URL(string: imageUrl), placeholderImage: placeholderImage, completed: nil)
        } else {
            photoImageView.image = placeholderImage
        }
    }
}
