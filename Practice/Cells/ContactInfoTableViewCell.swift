//
//  ContactInfoTableViewCell.swift
//  Practice
//
//  Created by Cypress on 10/17/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import UIKit

class ContactInfoTableViewCell: UITableViewCell {

    @IBOutlet private weak var avatarImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    
    func updateData(name: String?, avatar data: Data?) {
        nameLabel.text = name ?? ""
        avatarImageView.contentMode = .scaleAspectFill

        if data != nil {
            avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
            avatarImageView.layer.masksToBounds = true
            avatarImageView.image = UIImage(data: data!)
        } else {
            avatarImageView.layer.cornerRadius = 0.0
            avatarImageView.layer.masksToBounds = false
            avatarImageView.image = UIImage(named: Constants.iconNameUser)
        }
    }
}
