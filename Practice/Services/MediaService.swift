//
//  MediaService.swift
//  Practice
//
//  Created by Cypress on 10/21/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import MediaPlayer

class MediaService {

    static func getMediaFilesFrom(path pathComponent: String) throws -> [MediaFile] {
        guard let documentURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return [] }
        let path = documentURL.appendingPathComponent(pathComponent).absoluteURL
        let directoryContents = try FileManager.default.contentsOfDirectory(at: path, includingPropertiesForKeys: nil, options: [])
        
        let mediaFiles = directoryContents.filter { MediaService.isMediaFile(url: $0) }
        let result = mediaFiles.map { (url) -> MediaFile in
            let name = MediaService.mediaFileName(url)
            let type = MediaService.mediaFileType(url)
            let player = MediaPlayer(player: AVPlayer(url: url))
            
            return MediaFile(name: name, type: type, player: player, fileUrl: url) }

        return result
    }
    
    static private func mediaFileType(_ url: URL) -> MediaFileType {
        if url.pathExtension == "mp4" || url.pathExtension == "3gp" {
            return MediaFileType.video
        }

        return MediaFileType.audio
    }
    
    static private func mediaFileName(_ url: URL) -> String {
        let components = url.lastPathComponent.components(separatedBy: ".")
        let name = components.count > 0 ? components.first! : ""
        
        return name
    }
    
    static private func isMediaFile(url: URL) -> Bool {
        let pathExtention = url.pathExtension

        switch pathExtention {
        case "mp4","3gp","mp3","m4a","webm":
            return true
        default:
            return false
        }
    }

}
