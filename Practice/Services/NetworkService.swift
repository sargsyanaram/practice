//
//  NetworkService.swift
//  Practice
//
//  Created by Cypress on 10/14/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation

class NetworkService {
    static func getRequest(url urlString: String, parameters: [String:String]?, completionHandler: @escaping (Data?, Error?) -> Void) {

        guard let url = NetworkService.url(string: urlString, queryParameters: parameters) else { return }
        var urlRequest = URLRequest(url: url)
        let urlSession = URLSession(configuration: .default)
        
        urlRequest.httpMethod = "GET"
        urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
        urlSession.dataTask(with: urlRequest) { (responseData, response, error) in
            guard let responseData = responseData,
                let response = response as? HTTPURLResponse,
                error == nil else {
                    assertionFailure("error - \(error?.localizedDescription ?? "Unknown error")")
                    completionHandler(nil,error)
                    return
            }

            guard response.statusCode == 200 else {
                assertionFailure("error - \(response)")
                completionHandler(nil,error)
                return
            }
            
            completionHandler(responseData,nil)
        }.resume()
    }
    
    private static func url(string: String?, queryParameters: [String:String]?) -> URL? {
        
        if let queryParameters = queryParameters, let string = string {
            
            var components = URLComponents(string: string)
            var queryItems = [URLQueryItem]()

            for (key, _) in queryParameters {
                queryItems.append(URLQueryItem(name: key, value: queryParameters[key]))
            }

            components?.queryItems = queryItems
            return components?.url

        } else if let string = string {
            return URL(string: string)
        }

        return nil
    }    
}
