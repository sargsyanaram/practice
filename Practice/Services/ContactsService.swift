//
//  ContactsService.swift
//  Practice
//
//  Created by Cypress on 10/16/19.
//  Copyright © 2019 Cypress. All rights reserved.
//

import Foundation
import ContactsUI

class ContactsService {

    private static let keysToFetch = [
        CNContactFormatter.descriptorForRequiredKeys(for: .fullName),
        CNContactPhoneNumbersKey,
        CNContactEmailAddressesKey,
        CNContactThumbnailImageDataKey,
        CNContactImageDataKey] as [Any]

    static func getContacts() throws -> [Contact] {

        let contactStore = CNContactStore()
        let keysToFetch = ContactsService.keysToFetch
        var contacts: [CNContact] = []
        let allContainers: [CNContainer] = try contactStore.containers(matching: nil)

        for container in allContainers {
            let fetchPredicate = CNContact.predicateForContactsInContainer(withIdentifier: container.identifier)

            let containerResults = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
            contacts.append(contentsOf: containerResults)
        }

        let result = contacts.map { Contact(contact: $0) }
        return result
    }

    static func deleteContact(id: String) throws -> (Bool, String?) {
        let contactStore = CNContactStore()
        let fetchPredicate = CNContact.predicateForContacts(withIdentifiers: [id])

        let contacts = try contactStore.unifiedContacts(matching: fetchPredicate, keysToFetch: [])
        guard let contact = contacts.first else { return (false, Constants.messageContactNotFound) }

        let request = CNSaveRequest()
        let mutableContact = contact.mutableCopy() as! CNMutableContact

        request.delete(mutableContact)
        try contactStore.execute(request)

        return (true, nil)
    }

    static func editContact(_ contactInfo: Contact) throws -> (Bool, String?) {
        let store = CNContactStore()
        let fetchPredicate = CNContact.predicateForContacts(withIdentifiers: [contactInfo.id])
        let keysToFetch = ContactsService.keysToFetch

        let contacts = try store.unifiedContacts(matching: fetchPredicate, keysToFetch: keysToFetch as! [CNKeyDescriptor])
        guard let contact = contacts.first else { return (false, Constants.messageFailedToSaveChanges) }

        let mutableContact = contact.mutableCopy() as! CNMutableContact
        mutableContact.givenName = contactInfo.firstName
        mutableContact.familyName = contactInfo.lastName
         
        let phoneNumbers = contactInfo.phoneNumbers.map { CNLabeledValue(label: $0.label, value: CNPhoneNumber(stringValue:$0.value)) }
        mutableContact.phoneNumbers = phoneNumbers

        let emailAddresses = contactInfo.emails.map { CNLabeledValue(label: $0.label, value: $0.value as NSString) }
        mutableContact.emailAddresses = emailAddresses

        let saveRequest = CNSaveRequest()
        saveRequest.update(mutableContact)
        try store.execute(saveRequest)
        
        return (true, nil)
    }
}
